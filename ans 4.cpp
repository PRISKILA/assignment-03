#include <stdio.h>

int main()
{
	int number, i;
	printf("Enter a positive integer: ");
	scanf("%d", &number);
	printf("Factors of %d are:", number);
	
	i=1;
	while(i<=number){
		if (number%i ==0) 
		printf("%d",i);
		i++;
	}
	
	printf("\n");
	return 0;
}
